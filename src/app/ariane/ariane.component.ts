import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ariane',
  templateUrl: './ariane.component.html',
  styleUrls: ['./ariane.component.scss']
})
export class ArianeComponent implements OnInit {

  ariane : String = "fildariane/toto";

  constructor() { }

  ngOnInit() {
  }

}
