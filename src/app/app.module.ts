import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ArtistesComponent } from './artistes/artistes.component';
import { LogComponent } from './log/log.component';
import { RegisterComponent } from './register/register.component';
import { FooterComponent } from './footer/footer.component';
import { ArianeComponent } from './ariane/ariane.component';
import { ArtisteAlphaComponent } from './artistes/artiste-alpha/artiste-alpha.component';
import { ArtisteComponent } from './artiste/artiste.component';
import { AideComponent } from './aide/aide.component';

const appRoutes : Routes = [
  { path: 'artistes', component: ArtistesComponent },
  { path: 'log', component: LogComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'artiste', component: ArtisteComponent },
  { path: 'aide', component: AideComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ArtistesComponent,
    LogComponent,
    RegisterComponent,
    FooterComponent,
    ArianeComponent,
    ArtisteAlphaComponent,
    ArtisteComponent,
    AideComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
