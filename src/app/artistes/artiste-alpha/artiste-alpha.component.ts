import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-artiste-alpha',
  templateUrl: './artiste-alpha.component.html',
  styleUrls: ['./artiste-alpha.component.scss']
})
export class ArtisteAlphaComponent implements OnInit {

  url : String = "../../../assets/img/3.jpg";

  constructor() { }

  ngOnInit() {
  }

}
