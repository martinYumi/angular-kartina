import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtisteAlphaComponent } from './artiste-alpha.component';

describe('ArtisteAlphaComponent', () => {
  let component: ArtisteAlphaComponent;
  let fixture: ComponentFixture<ArtisteAlphaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtisteAlphaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtisteAlphaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
